Hooks.on("ready", () => {
    let babele_dir = "modules/babele-ja/translations";
    if (!game.settings.settings.hasOwnProperty("babele.directory")) {
        console.error(`babele-ja | ${game.i18n.localize("babele-ja.NoBabele")}`)
        return ui.notifications.warn(game.i18n.localize("babele-ja.NoBabele"));
    }
    if (game.settings.get("babele", "directory") !== babele_dir)
        game.settings.set("babele", "directory", babele_dir);
});