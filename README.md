### Babele-ja

This modules provides japanese language translation files used by the babele module.

### Usage
Install the module and activate it. The provided translations will be automatically loaded.

### Installation.
Use foundry module installation with the follwoing link for version 0.4.4
https://gitlab.com/tposney/babele-ja/raw/master/module.json"
Use foundry module installation with the follwoing link for version 0.4.0 - 0.4.3
"https://gitlab.com/tposney/babele-ja/raw/master/foundry0.4/module.json"
